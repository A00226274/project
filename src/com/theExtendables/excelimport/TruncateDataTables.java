package com.theExtendables.excelimport;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * Simple runner class to clear the data from the tables 
 * so they can be rewritten for testing purposes.
 */
public class TruncateDataTables {

	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		
		for (String tableName: MainDriver.sheetTableMap.values()) {
			truncateTables(tableName);
		}
		
		long endTime = System.currentTimeMillis();

		System.out.println("Total time: " + (endTime - startTime) + " milliseconds");

	}

	public static void truncateTables(String tableName) {
		Statement stmt;
		Connection conn;
		try {
			conn = ConnectionHelper.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("TRUNCATE " + tableName);
			System.out.println("Truncated table: " + tableName);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
