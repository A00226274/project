package com.theExtendables.excelimport;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MainDriver {
	private static File convertedFile;
	public static final Map<String, String> sheetTableMap; 
	static
	{
		sheetTableMap = new HashMap<String, String>();
		sheetTableMap.put("MCC - MNC Table", "mcc_mnc");
		sheetTableMap.put("UE Table", "ue");
		sheetTableMap.put("Event-Cause Table", "event_cause");
		sheetTableMap.put("Base Data", "sampledata");
		sheetTableMap.put("Failure Class Table", "failure_class");
	}
	public static void main(String[] args) throws SQLException{
		importToDatabase();
	}
	private static void importToDatabase() throws SQLException{
		XlsToCsvConverter converter = new XlsToCsvConverter();
		File xlsFIle = new File("AIT Group Project - Sample Dataset.xls");
		Connection connection = ConnectionHelper.getConnection();
		long startTime = System.currentTimeMillis();
		for (Map.Entry entry : sheetTableMap.entrySet()) {
			convertedFile = converter.convertFile(xlsFIle, entry.getKey().toString());
			WriteCSVToDB.importDataToDB(connection, convertedFile.getName(), entry.getValue().toString());
		}
		System.out.println("Total time: " + (System.currentTimeMillis() - startTime) + " milliseconds");
		if(convertedFile.delete()){
			System.out.println(convertedFile.getName() + " has been deleted!");
		}else{
			System.out.println("Delete operation failed.");
		}
	}
}