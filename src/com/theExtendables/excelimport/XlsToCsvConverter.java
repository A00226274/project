package com.theExtendables.excelimport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Locale;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;

public class XlsToCsvConverter {
	private File outputCSVFile = new File("tempdata.csv");

	public File convertFile(File inputFile, String tableName) 
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD HH:mm:SS");
		long startTime = System.currentTimeMillis(),d;
		StringBuffer data = new StringBuffer();
		HSSFWorkbook workbook = null;
		java.util.Date dt;
		String currentTime;
		try 
		{
			FileOutputStream fos = new FileOutputStream(outputCSVFile);
			workbook = new HSSFWorkbook(new FileInputStream(inputFile));
			HSSFSheet sheet = workbook.getSheet(tableName);
			if (sheet == null) {
				System.out.println("Not a valid sheet");
				fos.close();
				throw new RuntimeException();
			}
			Cell cell;
			Row row;
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) 
			{
				row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) 
				{
					cell = cellIterator.next();
					switch (cell.getCellType()) 
					{
					case Cell.CELL_TYPE_NUMERIC:{
						if (DateUtil.isCellDateFormatted(cell)) {
							dt = cell.getDateCellValue();
							currentTime = sdf.format(dt);
							data.append(currentTime+ ";");
							break;
						} else {
							d = (long)cell.getNumericCellValue();
							data.append(d + ";");
							break;
						}
					}
					case Cell.CELL_TYPE_STRING:
						if (cell.getStringCellValue().contains("(null)")) {
							data.append("\\N;");
						}
						else{
							data.append(cell.getStringCellValue() + ";");
						}
						break;
					case Cell.CELL_TYPE_BLANK:
						data.append("");
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						data.append(cell.getBooleanCellValue() + ";");
						break;
					default:
						data.append(cell + ";");
					}
				}
				data.append('\n'); 
			}
			fos.write(data.toString().getBytes());
			fos.close();
			workbook.close();
			System.out.println("Converting "+ tableName +" to .csv took: "+ (System.currentTimeMillis() - startTime) +" milliseconds");
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return outputCSVFile;
	}
}
